#!/bin/bash

/sbin/sysctl -w net.ipv4.ip_forward=1

iptables -t nat \
	 -A POSTROUTING \
	 -s 192.9.0.0/24 \
	 -o eno1 \
	 -j MASQUERADE
iptables \
    -A FORWARD \
    -i eno1 -o eno2 \
    -m state \
    --state RELATED,ESTABLISHED \
    -j ACCEPT
iptables \
    -A FORWARD \
    -i eno2 -o eno1 \
    -j ACCEPT
