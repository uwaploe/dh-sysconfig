#!/usr/bin/env bash

vers="$(git describe --tags --always --dirty --match=v* 2> /dev/null)"
VERSION="${vers#v}"
export VERSION
nfpm pkg --target must-dh-sys_${VERSION}_any.deb
